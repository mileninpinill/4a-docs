package com.invenfarma.invenfarma_ms.repositories;

import com.invenfarma.invenfarma_ms.models.Productos;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface ProductosRepository extends MongoRepository<Productos, String> {
    List<Productos> findByUsernameOrigin (String usernameOrigin);
    List<Productos> findByUsernameDestiny (String usernameDestiny);
}

