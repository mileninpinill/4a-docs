package com.invenfarma.invenfarma_ms.repositories;

import com.invenfarma.invenfarma_ms.models.Proveedores;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProveedoresRepository extends MongoRepository <Proveedores,String> { }
