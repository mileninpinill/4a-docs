package com.invenfarma.invenfarma_ms.exceptions;

public class ProveedoresNotFoundException extends RuntimeException {
    public ProveedoresNotFoundException(String message) {
        super(message);
    }
}