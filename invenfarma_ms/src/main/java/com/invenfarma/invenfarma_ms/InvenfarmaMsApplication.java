package com.invenfarma.invenfarma_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InvenfarmaMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(InvenfarmaMsApplication.class, args);
	}

}
