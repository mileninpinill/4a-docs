package com.invenfarma.invenfarma_ms.controllers;


import com.invenfarma.invenfarma_ms.exceptions.ProveedoresNotFoundException;
import com.invenfarma.invenfarma_ms.models.Proveedores;
import com.invenfarma.invenfarma_ms.repositories.ProveedoresRepository;
import org.springframework.web.bind.annotation.*;


@RestController
class ProveedoresController {

    private final ProveedoresRepository proveedoresRepository;

    public ProveedoresController (ProveedoresRepository proveedoresRepository) {
        this.proveedoresRepository = proveedoresRepository;
    }
    @GetMapping("/proveedores/{username}")
    Proveedores getProveedores(@PathVariable String username){
        return proveedoresRepository.findById(username)
                .orElseThrow(() -> new ProveedoresNotFoundException
                        ("No se encontro una cuenta con el username: " + username));
    }
    @PostMapping("/proveedores")
    Proveedores newProveedores(@RequestBody Proveedores proveedores){
        return proveedoresRepository.save(proveedores);
    }
}