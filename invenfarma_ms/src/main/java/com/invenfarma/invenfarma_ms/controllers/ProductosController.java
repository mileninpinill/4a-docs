package com.invenfarma.invenfarma_ms.controllers;

import com.invenfarma.invenfarma_ms.exceptions.ProveedoresNotFoundException;
import com.invenfarma.invenfarma_ms.exceptions.InsufficientBalanceException;
import com.invenfarma.invenfarma_ms.models.Productos;
import com.invenfarma.invenfarma_ms.models.Proveedores;
import com.invenfarma.invenfarma_ms.repositories.ProductosRepository;
import com.invenfarma.invenfarma_ms.repositories.ProveedoresRepository;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class ProductosController {

    private final ProveedoresRepository proveedoresRepository;
    private final ProductosRepository productosRepository;

    public ProductosController (ProveedoresRepository proveedoresRepository,
                                 ProductosRepository productosRepository) {
        this.proveedoresRepository = proveedoresRepository;
        this.productosRepository = productosRepository;
    }
    @PostMapping("/productos")
    Productos newProductos(@RequestBody Productos productos){
        Proveedores proveedoresOrigin =
                proveedoresRepository.findById(productos.getUsernameOrigin()).orElse(null);
        Proveedores proveedoresDestinity=
                proveedoresRepository.findById(productos.getUsernameDestiny()).orElse(null);

        if (proveedoresOrigin == null)
            throw new ProveedoresNotFoundException("No se encontro una cuenta con el username:"
                     + productos.getUsernameOrigin());
        if (proveedoresDestinity == null)
            throw new ProveedoresNotFoundException("No se encontro una cuenta con el username:"
                     + productos.getUsernameDestiny());
        if(proveedoresOrigin.getBalance() < productos.getValue())
            throw new InsufficientBalanceException("Saldo Insuficiente");
        proveedoresOrigin.setBalance(proveedoresOrigin.getBalance() - productos.getValue());
        proveedoresOrigin.setLastChange(new Date());
        proveedoresRepository.save(proveedoresOrigin);

        proveedoresDestinity.setBalance(proveedoresDestinity.getBalance() +
                productos.getValue());
        proveedoresDestinity.setLastChange(new Date());
        proveedoresRepository.save(proveedoresDestinity);

        productos.setDate(new Date());
        return productosRepository.save(productos);
    }
    @GetMapping("/productos/{username}")
    List<Productos> userproductos(@PathVariable String username){
        Proveedores userproveedores = proveedoresRepository.findById(username).orElse(null);
        if (userproveedores == null)

        throw new ProveedoresNotFoundException("No se encontro una cuenta con el username:"
                 + username);
                List<Productos> productosOrigin =
                        productosRepository.findByUsernameOrigin(username);
        List<Productos> productosDestinity =
                productosRepository.findByUsernameDestiny(username);
        List<Productos> productos = Stream.concat(productosOrigin.stream(),
                productosDestinity.stream()).collect(Collectors.toList());
        return productos;
    }
}