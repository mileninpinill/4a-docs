//pedimos apolo server
const { ApolloError } = require('apollo-server');
//ubicación de las apis rest de los backends
const serverConfig = require('../server');
//fetch (libreria para hacer llamados por datos)
const fetch = require('node-fetch');


//logica para el servicio de la autenticación (Python)

// sera asincronica - pedimos el token de acceso si no viene quedara vacio -
const authentication = async ({ req }) => {
    const token = req.headers.authorization || '';

    // si no recibe ningun token se ejecutara el condicional
    if (token == '')
        return { userIdToken: null }

    // tiene que recibir un metodo post, token tipo json y un body con un json token.(Como si fuera postman)    
    else {
        try {
            let requestOptions = {
                method: 'POST', 
                headers: { "Content-Type": "application/json" },
                body: JSON.stringify({ token }), redirect: 'follow'
        };
    
    //variable que espera(await) el servicio web asincrono 
    let response = await fetch(
        //colocamos la ruta del server config y de la api del backend (python verifyToken)
        `${serverConfig.auth_api_url}/verifyToken/`,
        requestOptions)

    //condicional de que algo fallo en la validación e imprima el error en la terminal.    
    if (response.status != 200) {
        console.log(response)
        //dira un error en el front, error 401 
        throw new ApolloError(`SESION INACTIVA - ${401}` + response.status, 401)
    }

    //la respuesta con el token del usuario, await de un servicio asincrono
    return { userIdToken: (await response.json()).UserId };
    }

    // catch de un error 
    catch (error) {
        throw new ApolloError(`TOKEN ERROR: ${500}: ${error}`, 500);
    }

    }
}

//agregar a todo el proyecto el modulo
module.exports = authentication;


