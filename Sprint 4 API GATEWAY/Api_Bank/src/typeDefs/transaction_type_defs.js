const { gql } = require('apollo-server');

/*
1- datos del model de transacción
2- la transacción como tal - origen, destino y valor
3- query de la busqueda
4- mutacion de la transaccion */

const transactionTypeDefs = gql `
    type Transaction {
        id: String!
        usernameOrigin: String!
        usernameDestiny: String!
        value: Int!
        date: String!
    }

    input TransactionInput {
        usernameOrigin: String!
        usernameDestiny: String!
        value: Int!
    }

    extend type Query {
        transactionByUsername(username: String!): [Transaction]
    }

    extend type Mutation {
        createTransaction(transaction: TransactionInput!): Transaction
    }

`;
module.exports = transactionTypeDefs;