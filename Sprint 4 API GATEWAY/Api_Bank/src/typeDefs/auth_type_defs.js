 //llamamos al graphql del apolo
const { gql } = require('apollo-server');

//tipos de datos que tendremos en la autenticacion- entradas y salidas de datos
/* 1- autenticación del usuario refresh y acceso,
2- token de acceso
3- Credential login: user y pass
4- SignUpInput: registro del usuario
5- UserDetail detalle usuario 
6- Query: obtener información del usuario
7- Mutation modificación en la base de datos: crear usuario, login, token de acceso*/  

const authTypeDefs = gql `

    type Tokens {
        refresh: String!
        access: String!
    }

    type Access {
        access: String!
    }

    input CredentialsInput {
        username: String!
        password: String!
    }

    input SignUpInput {
        username: String!
        password: String!
        name: String!
        email: String!
        balance: Int!
    }

    type UserDetail {
        id: Int!
        username: String!
        password: String!
        name: String!
        email: String!
    }

    type Mutation {
        signUpUser(userInput :SignUpInput): Tokens!
        logIn(credentials: CredentialsInput!): Tokens!
        refreshToken(refresh: String!): Access!
    }

    type Query {
        userDetailById(userId: Int!): UserDetail!
    }
`;

module.exports = authTypeDefs;