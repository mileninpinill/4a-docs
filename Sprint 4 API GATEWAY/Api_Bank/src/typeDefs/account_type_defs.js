//apolo
const { gql } = require('apollo-server');


// datos de la cuenta 
const accountTypeDefs = gql `

type Account {
    username  : String!
    balance   : Int!
    lastChange: String!
}

extend type Query {
    accountByUsername(username: String!): Account
}

`;
module.exports = accountTypeDefs;