const { RESTDataSource } = require('apollo-datasource-rest');
const serverConfig = require('../server');

//api de java 
class AccountAPI extends RESTDataSource {
    constructor() {
        super();
        this.baseURL = serverConfig.account_api_url;
    }

//servicios 
async createAccount(account) {
    account = new Object(JSON.parse(JSON.stringify(account)));
    return await this.post('/accounts', account);
}


async accountByUsername(username) {
    return await this.get(`/proveedores/${username}`);
}


async createTransaction(transaction) {
    transaction = new Object(JSON.parse(JSON.stringify(transaction)));
    return await this.post('/productos', transaction);
}


async transactionByUsername(username) {
    return await this.get(`/productos/${username}`);
}


}

module.exports = AccountAPI;