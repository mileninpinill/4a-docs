//llamado tipo rest funciona como el ajax de vue
const { RESTDataSource } = require('apollo-datasource-rest');

//llamado server
const serverConfig = require('../server');

//servicios python
class AuthAPI extends RESTDataSource {
    
    //constructor que invoca la url de la api a conectar
    constructor() {
        super();
        this.baseURL = serverConfig.auth_api_url;
    } 

    //crear usuario 
    async createUser(user) {
        user = new Object(JSON.parse(JSON.stringify(user)));
            return await this.post(`/user/`, user);
    }

    //datos del usuario
    async getUser(userId) {
        return await this.get(`/user/${userId}/`);
    }

    
    async authRequest(credentials) {
        credentials = new Object(JSON.parse(JSON.stringify(credentials)));
            return await this.post(`/login/`, credentials);
    }


    async refreshToken(token) {
        token = new Object(JSON.parse(JSON.stringify({ refresh: token })));
            return await this.post(`/refresh/`, token);
    }

}

module.exports = AuthAPI;